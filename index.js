var express = require('express');
var path = require('path');
var app = express();
var Routes = require('./route/route');
var bodyparser = require('body-parser');

app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());

app.use('/singlefoto', express.static('singlefoto'));
app.use('/photo', express.static('photo'));

app.use('/v1', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, Content-Type, Accept, Authorization, Content-Length, Date, X-Api-Version, x-access-token, X-Response-Time, X-PINGOTHER ");
  next();
}, Routes);

app.listen(process.env.PORT, function() {
  console.log("back end jalan")
});
