var express = require('express');
var app = express();
var knex = require('knex');
var bodyparser = require('body-parser');
var db = require('../koneksi/connection');
var Agenda = require('../model/agenda');
var gallery = require('../model/gallery');
var jwt_key = require('../config/configAuth');
var jwt_config = require('../middleware/auth');
var Routes = express.Router();
let multer = require('multer');
let upload = multer();
var joi = require('joi');
var ekspressJwt = require('express-jwt');
const agendaController   = require('../controllers/agendaControllers');
const galleryControllers = require('../controllers/galleryControllers');
const beritaController = require('../controllers/beritaControllers');
const userController = require('../controllers/userController');
const saranController = require('../controllers/saranController');

let auth = function(req, res, next) {
	console.log(req.header('Authorization'))
	next();
}

app.use(ekspressJwt({ secret: jwt_key}).unless({ path: ['/v1/checklogin']}));

Routes.get('/', function(req, res, next){
	res.status(200).json({'result': true, 'data': 'komunitaspijar-api'})
})

Routes.get('/cektoken', jwt_config, agendaController.ControllerViewAgendaAll);

Routes.get('/listagenda/:page', agendaController.ControllerViewAgenda);
Routes.post('/addagenda', agendaController.ControllerAddAgenda);
Routes.post('/addgallery', galleryControllers.addGallery);
Routes.get('/listagendaall', agendaController.ControllerViewAgendaAll);
Routes.post('/addberita', beritaController.AddBeritaController);
Routes.get('/listberitaagama/:page', beritaController.BeritaAgamaController);
Routes.get('/listberitahumaniora/:page', beritaController.BeritaHumaioraController);
Routes.get('/listberitapendidikan/:page', beritaController.BeritaPendidikanController);
Routes.get('/listberitasosial/:page', beritaController.BeritaSosisalController);
Routes.get('/findagenda/:id', agendaController.ControllerFindAgenda);
Routes.get('/listgallery/:page', galleryControllers.listGalleryController);
Routes.get('/listberitaall/:page', beritaController.AllBeritaController);
Routes.get('/findberita/:id', beritaController.FindBeritaController);
//Routes.post('/adduser', upload.fields([]), userController.AddUserControllerDua);
Routes.post('/adduser', userController.AddUserControllerDua);
Routes.get('/listuser/:page', userController.AllUserController);
Routes.get('/finduser/:id', userController.FindBeritaController);
Routes.post('/updateuser', userController.UpdateUserController);
Routes.post('/checklogin', userController.CheckLoginController);
Routes.post('/updateagenda', agendaController.ControllerUpdateAgenda);
Routes.post('/deleteuser', userController.DeleteUserController);
Routes.post('/deleteagenda', agendaController.ControllerDeleteAgenda);
Routes.post('/updateberita', beritaController.UpdateBeritaController);
Routes.post('/deleteberita', beritaController.DeleteBeritaController);
Routes.post('/addsaran', saranController.AddSaranController);
Routes.get('/galleryhome', galleryControllers.GalleryHomeController);
Routes.get('/beritahome', beritaController.BeritaHomeController);

module.exports = Routes

