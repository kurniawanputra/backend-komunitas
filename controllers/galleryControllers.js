var express = require('express');
var knex = require('knex');
var bodyparser = require('body-parser');
var db = require('../koneksi/connection');
var Gallery = require('../model/gallery');
var Routes = express.Router();
var joi = require('joi');
var multer = require('multer');
var path = require('path');
var crypto = require('crypto');

var storage = multer.diskStorage({
  destination: function (request, file, callback) {
    callback(null, 'photo/');
  },
  filename: function (request, file, callback) {
    crypto.pseudoRandomBytes(16, function (err, raw) {
      if (err) return cb(err)
      callback(null, raw.toString('hex') + path.extname(file.originalname))
    });
  }
});
var uploadArray = multer({ storage: storage }).array('data[]', 5);

function addGallery(request, response) {
  uploadArray(request, response, err => {
    if (err) {
      console.log("Error oncured");
      response.status(400).json({
        status: "error",
        message: "file to upload",
        error: err
      });
    } else {
      const arrayNew = [];
      console.log('====');
      console.log(request);
      request.files.forEach((photo) => {
        arrayNew.push({
          judul_gallery: photo.originalname,
          url_gallery: photo.filename
        })
      })
      console.log(arrayNew);
      Gallery
        .addGallery(
        arrayNew
        )
        .then(result => {
          response.status(200).json({
            "message": "success input data",
            "status": "success",
            "data": request.files
          })
        })
        .catch(err => {
          response.status(400).json({
            "status": "error ke db",
            "error": err.message,
            "data": request.body
          })
        })
    }
  });
}

function listGalleryController(req, res) {
  const limit = 10;
  const offset = (limit * parseInt(req.params.page)) - limit;// 0, 10, 20
  Gallery.listGallery((error, data, total) => {
    if (error) res.status(400).json({ 'message': 'error', 'error': error });
    console.log(data);
    res.status(200).json({ 'message': 'success', 'data': data, page: parseInt(req.params.page), limit: limit, total });
  }, limit, offset);
}

function GalleryHomeController(req, res) {
  Gallery.GalleryHome((error, data) => {
    if (error) {
      res.status(400).json({
        'message' : 'gagal tampilkan data',
        'error' : error
      })
    } else {
      res.status(200).json({
        'message' : 'sukses tampil data',
        'data' : data
      })
    }
  })
}


module.exports = {
  addGallery: addGallery,
  listGalleryController: listGalleryController,
  GalleryHomeController: GalleryHomeController
}