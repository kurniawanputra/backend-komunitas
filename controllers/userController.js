var express = require('express');
var knex = require('knex');
var bodyparser = require('body-parser');
var db = require('../koneksi/connection');
var User = require('../model/user');
var Routes = express.Router();
var joi = require('joi');
var path = require('path');
var fs = require('fs');
var jwt_key = require('../config/configAuth');
var jwt = require('jsonwebtoken');

function AddUserController(req, res) {
  var username = req.body.username;
  var password = req.body.password;
  if (username === "" || password === "") {
    console.log(req.body.username);
    res.status(400).json({ 'message': 'error 400', 'error': errors });
  } else {
    User
      .InsertUser({ username, password })
      .then(result => {
        res.status(200).json({
          "status": "success",
          "message": "success insert to data",
          "data": req.body
        })
      })
      .catch(error => {
        res.status(400).json({
          "status": "error",
          "error": req.body.username
        })
      })
  }
}

function AddUserControllerDua(req, res) {
  const objectValidation = joi.object().keys({
    username: joi.string().required(),
    password: joi.string().required()
  });
  joi.validate({
    username: req.body.username,
    password: req.body.password
  }, (error, result) => {
    console.log(result);
    if (error) {
      res.status(400).json({ 'message': 'Terjadi Kesalahan', 'error': error.message });
    } else {
      User
        .InsertUser(result)
        .then(result => {
          res.status(200).json({
            "message": "success insert data",
            "status": "success",
            "data": req.body
          })
        })
        .catch(err => {
          res.status(400).json({
            "status": "error",
            "error": err.message,
            "data": req.body
          })
        })
    }
  })
}

function CheckLoginController(req, res) {
  const objectValidation = joi.object().keys({
    username: joi.string().required(),
    password: joi.string().required()
  });
  joi.validate({
    username: req.body.username,
    password: req.body.password
  }, (error, result) => {
    User
      .checkLogin(result)
      .then(result => {
        var token = jwt.sign(req.body, jwt_key.secret);
        res.status(200).json({
          "message": "success login",
          "token" : token,
          "status": "success",
          "data": req.body
        })
      })
      .catch(err => {
        res.status(400).json({
          "status": "error",
          "error": err,
          "data": req.body
        })
      })
  })
}

function UpdateUserController(req, res) {
  const objectValidation = joi.object().keys({
    id_user: joi.number().required(),
    username: joi.string().required(),
    password: joi.string().required(),
    baru: joi.string().required()
  });
  joi.validate({
    id_user: req.body.id_user,
    username: req.body.username,
    password: req.body.password,
    baru: req.body.baru
  }, (error, result) => {
    if (error) {
      res.status(400).json({ 'message': 'Terjadi Kesalahan', 'error': error.message });
    } else {
      User
        .updateUser(result)
        .then(result => {
          res.status(200).json({
            "message": "success update data",
            "status": "success",
            "data": req.body
          })
        })
        .catch(err => {
          res.status(400).json({
            "status": "error",
            "error": err,
            "data": req.body
          })
        })
    }
  })

}

function AllUserController(req, res) {
  const limit = 10;
  const offset = (limit * parseInt(req.params.page)) - limit;// 0, 10, 20
  User.listUser((error, data, total) => {
    if (error) res.status(400).json({ 'message': 'error', 'error': error });
    console.log(data);
    res.status(200).json({ 'message': 'success', 'data': data, page: parseInt(req.params.page), limit: limit, total });
  }, limit, offset);
}

function FindBeritaController(req, res) {
  var id = req.params.id;
  User.findUser(id, (error, data) => {
    if (error) res.status(400).json({
      'message': 'error',
      'error': error
    })
    console.log(data);
    res.status(200).json({
      message: 'success',
      'data': data
    })
  })
}

function DeleteUserController(req, res) {
  const objectValidation = joi.object().keys({
    id_user : joi.number().required()
  })
  joi.validate({
    id_user : req.body.id_user
  }, (error, result) => {
    if (error) {
      res.status(400).json({
        'message' : 'terjadi kesalahan',
        'error' : error.message
      })
    } else {
      User
        .deleteUser(result)
        .then(result => {
          res.status(200).json({
            'message' : 'success delete user',
            'status' : 'success',
            'data' : req.body
          })
        })
        .catch(err => {
          res.status(400).json({
            'message' : 'terjasi kesalahan saat error',
            'error' : err,
            'data' : req.body
          })
        })
    }
  })
}

module.exports = {
  AddUserController    : AddUserController,
  AddUserControllerDua : AddUserControllerDua,
  AllUserController    : AllUserController,
  FindBeritaController : FindBeritaController,
  UpdateUserController : UpdateUserController,
  CheckLoginController : CheckLoginController,
  DeleteUserController : DeleteUserController
}