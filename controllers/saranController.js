var express = require('express');
var knex = require('knex');
var bodyparser = require('body-parser');
var db = require('../koneksi/connection');
var Saran = require('../model/saran');
var Routes = express.Router();
var joi = require('joi');

function AddSaranController(request,respond) {
  const objectValidation = joi.object().keys({
    nama : joi.string().required(),
    email : joi.string().required(),
    subyek : joi.string().required(),
    pesan : joi.string().required()
  })
  joi.validate({
    nama : request.body.nama,
    email : request.body.email,
    subyek : request.body.subyek,
    pesan : request.body.pesan
  }, (error, result) => {
    if (error) {
      respond.status(400).json({
        'message' : 'terjadi kesalahan tambah saran',
        'error' : error.message
      })
    } else {
      Saran
        .AddSaran(result)
        .then(result => {
          respond.status(200).json({
            'message' : 'success tambah saran',
            'status' : 'success',
            'data' : request.body
          })
        })
        .catch(err => {
          respond.status(400).json({
            'message' : 'gagal input saran',
            'status' : 'failed',
            'data' : request.body
          })
        })
    }
  })
}

module.exports = {
  AddSaranController : AddSaranController
}