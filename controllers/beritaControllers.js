var express = require('express');
var knex = require('knex');
var bodyparser = require('body-parser');
var db = require('../koneksi/connection');
var Berita = require('../model/berita');
var Routes = express.Router();
var joi = require('joi');
var multer = require('multer');
var path = require('path');
var crypto = require('crypto');
var fs = require('fs');

var storage = multer.diskStorage({
  destination: function (request, file, callback) {
    callback(null, 'singlefoto/');
  },
  filename: function (request, file, callback) {
    callback(null, file.originalname);
  }
});
var upload = multer({ storage: storage }).single('file');

function AddBeritaController(request, response) {
  upload(request, response, (err) => {
    if (err) {
      console.log('Error Upload');
      response.status(400).json({
        status: "Error pada saat Upload",
        message: "File failed to upload",
        error: err
      })
    } else {
      Berita
        .AddBerita({
          judul_berita: request.body.judul_berita,
          deskripsi_berita: request.body.deskripsi_berita,
          isi_berita: request.body.isi_berita,
          kategori_berita: request.body.id_kategori_berita,
          image_berita: request.file.filename
        })
      response.status(200).json({
        status: "success",
        message: "File Uploaded",
        data: request.body
      })
    }
  })
}

function UpdateBeritaController(request, response) {
  upload(request, response, (err) => {
    if (!request.file) {
      Berita
        .UpdateBerita({
          id_berita: request.body.id_berita,
          judul_berita: request.body.judul_berita,
          deskripsi_berita: request.body.deskripsi_berita,
          isi_berita: request.body.isi_berita,
          kategori_berita: request.body.kategori_berita
        })
        .then(result => {
          response.status(200).json({
            status: "success",
            message: "succes update berita",
            data: request.body
          })
        })
        .catch(error => {
          response.status(400).json({
            status: "error",
            message: "error update data",
            data: request.body
          })
        })
    } else {
      Berita
        .UpdateBerita({
          id_berita: request.body.id_berita,
          judul_berita: request.body.judul_berita,
          deskripsi_berita: request.body.deskripsi_berita,
          isi_berita: request.body.isi_berita,
          kategori_berita: request.body.kategori_berita,
          image_berita: request.file.filename
        })
        .then(result => {
          response.status(200).json({
            status: "success",
            message: "succes update berita",
            data: request.body
          })
        })
        .catch(error => {
          response.status(400).json({
            status: "error",
            message: "error update data",
            data: request.body
          })
        })
    }
  })
}

function DeleteBeritaController(request, response) {
  const objectValidation = joi.object().keys({
    id_berita : joi.number().required()
  })
  joi.validate({
    id_berita : request.body.id_berita
  }, (error, result) => {
    if (error) {
      response.status(400).json({
        'message' : 'terjadi kesalahan',
        'error' : error.message
      })
    } else {
      Berita
        .DeleteBerita(result)
        .then(result => {
          response.status(200).json({
            'message' : 'success delete user',
            'status' : 'success',
            'data' : request.body
          })
        })
        .catch(err => {
          response.status(400).json({
            'message' : 'terjasi kesalahan saat error',
            'error' : err,
            'data' : request.body
          })
        })
    }
  })
}

function BeritaAgamaController(request, response) {
  const limit = 10;
  const offset = (limit * parseInt(request.params.page)) - limit;// 0, 10, 20
  Berita.BeritaAgama((error, data, total) => {
    if (error) response.status(400).json({ message: 'error', 'error': error });
    console.log(data);
    response.status(200).json({
      message: 'success',
      'data': data,
      page: parseInt(request.params.page),
      limit: limit,
      total
    })
  }, limit, offset);
}

function BeritaHumaioraController(request, response) {
  const limit = 10;
  const offset = (limit * parseInt(request.params.page)) - limit;//0, 10, 20
  Berita.BeritaHumaniora((error, data, total) => {
    if (error) response.status(400).json({ message: 'error', 'error': error });
    console.log(data);
    response.status(200).json({
      message: 'success',
      'data': data,
      page: parseInt(request.params.page),
      limit: limit,
      total
    })
  }, limit, offset);
}

function BeritaPendidikanController(request, response) {
  const limit = 10;
  const offset = (limit * parseInt(request.params.page)) - limit;//0, 10, 20
  Berita.BeritaPendidikan((error, data, total) => {
    if (error) response.status(400).json({ message: 'error', 'error': error });
    console.log(data);
    response.status(200).json({
      message: 'success',
      'data': data,
      page: parseInt(request.params.page),
      limit: limit,
      total
    })
  }, limit, offset);
}

function BeritaSosisalController(request, response) {
  const limit = 10;
  const offset = (limit * parseInt(request.params.page)) - limit;//0, 10, 20
  Berita.BeritaSosisal((error, data, total) => {
    if (error) response.status(400).json({ message: 'error', 'error': error });
    console.log(data);
    response.status(200).json({
      message: 'success',
      'data': data,
      page: parseInt(request.params.page),
      limit: limit,
      total
    })
  }, limit, offset);
}

function AllBeritaController(req, res) {
  const limit = 10;
  const offset = (limit * parseInt(req.params.page)) - limit;// 0, 10, 20
  Berita.AllBerita((error, data, total) => {
    if (error) res.status(400).json({ 'message': 'error', 'error': error });
    console.log(data);
    res.status(200).json({ 'message': 'success', 'data': data, page: parseInt(req.params.page), limit: limit, total });
  }, limit, offset);
}

function FindBeritaController(req, res) {
  var id = req.params.id;
  Berita.FindBerita(id, (error, data) => {
    if (error) res.status(400).json({
      'message': 'error',
      'error': error
    })
    console.log(data);
    res.status(200).json({
      message: 'success',
      'data': data
    })
  })
}

function BeritaHomeController(req, res) {
  Berita.BeritaHome((error, data) => {
    if (error) {
      res.status(400).json({
        'message' : 'gagal tampil data',
        'error' : error
      })
    } else {
      res.status(200).json({
        'message' : 'sukses tampil data',
        'data' : data
      })
    }
  })
}

module.exports = {
  AddBeritaController: AddBeritaController,
  BeritaAgamaController: BeritaAgamaController,
  BeritaHumaioraController: BeritaHumaioraController,
  BeritaPendidikanController: BeritaPendidikanController,
  BeritaSosisalController: BeritaSosisalController,
  AllBeritaController: AllBeritaController,
  FindBeritaController: FindBeritaController,
  UpdateBeritaController: UpdateBeritaController,
  DeleteBeritaController: DeleteBeritaController,
  BeritaHomeController: BeritaHomeController
}