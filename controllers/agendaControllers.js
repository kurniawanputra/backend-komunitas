var express = require('express');
var knex = require('knex');
var bodyparser = require('body-parser');
var db = require('../koneksi/connection');
var Agenda = require('../model/agenda');
var Routes = express.Router();
var joi = require('joi');
var moment = require('moment');

const controller = {
  ControllerViewAgenda: function (req, res) {
    const limit = 10;
    const offset = (limit * parseInt(req.params.page)) - limit;// 0, 10, 20
    Agenda.viewAgenda((error, data, total) => {
      if (error) res.status(400).json({ 'message': 'error', 'error': error });
      console.log(data);
      res.status(200).json({ 'message': 'success', 'data': data, page: parseInt(req.params.page), limit: limit, total });
    }, limit, offset);
  },

  ControllerFindAgenda: function (req, res) {
    var id = req.params.id;
    Agenda.findAgenda(id, (error, data) => {
      if (error) res.status(400).json({
        'message': 'error',
        'error': error
      });
      console.log(data);
      res.status(200).json({
        'message': 'success',
        'data': data
      });
    })
  },

  ControllerViewAgendaAll: function (req, res) {
    Agenda.viewAllAgenda((error, data) => {
      if (error) res.status(400).json({
        message: 'error',
        'error': error
      })
      console.log(data);
      res.status(200).json({
        message: 'success',
        'data': data
      })
    })
  },

  ControllerAddAgenda: function (req, res) {
    const objectValidation = joi.object().keys({
      title: joi.string().required(),
      start_date: joi.date().required(),
      end_date: joi.date().required(),
      desc: joi.string().required()
    });
    joi.validate({
      title: req.body.title,
      start_date: req.body.start_date,
      end_date: req.body.end_date,
      desc: req.body.desc
    }, (error, result) => {
      if (error) {
        res.status(400).json({ 'message': 'Terjadi Kesalahan', 'error': error.message });
      } else {
        Agenda
          .addAgenda(result)
          .then(result => {
            res.status(200).json({
              "message": "success input data",
              "status": "success",
              "data": req.body
            })
          })
          .catch(err => {
            res.status(400).json({
              "status": "error",
              "error": err.message,
              "data": req.body
            })
          })
      }
    })
  },

  ControllerUpdateAgenda: function (req, res) {
    const objectValidation = joi.object().keys({
      id_agenda: joi.number().required(),
      title: joi.string().required(),
      start_date: joi.date().required(),
      end_date: joi.date().required(),
      desc: joi.string().required()
    });
    joi.validate({
      id_agenda: req.body.id_agenda,
      title: req.body.title,
      start_date: req.body.start_date,
      end_date: req.body.end_date,
      desc: req.body.desc
    }, (error, result) => {
      if (error) {
        res.status(400).json({ 'message': 'Terjadi Kesalahan pada saat update', 'error': error.message });
      } else {
        Agenda
          .updateAgenda(result)
          .then(result => {
            res.status(200).json({
              "message": "success update data",
              "status": "success",
              "data": req.body
            })
          })
          .catch(err => {
            res.status(400).json({
              "status": "error",
              "error": err.message,
              "data": req.body
            })
          })
      }
    })

  },

  ControllerDeleteAgenda: function(req, res) {
    const objectValidation =  joi.object().keys({
      id_agenda : joi.number().required()
    });
    joi.validate({
      id_agenda : req.body.id_agenda
    }, (error, result) => {
      if (error) {
        res.status(400).json({
          'message' : 'terjadi kesalahan pada saat delete',
          'error' : error.message
        })
      } else {
        Agenda
          .deleteAgenda(result)
          .then(result => {
            res.status(200).json({
              'message' : 'berhasil delete data',
              'status' : 'success',
              'data' : req.body
            })
          })
          .catch(err => {
            res.status(400).json({
              'message' : 'gagal delete data',
              'error' : err.message
            })
          })
      }
    })
  }
}

module.exports = controller;

