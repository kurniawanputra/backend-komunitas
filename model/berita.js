var express = require('express');
var app = express();
var knex = require('knex');
var bosyparser = require('body-parser');
var db = require('../koneksi/connection');

function AddBerita(data) {
  return new Promise((resolve, reject) => {
    db('berita')
      .insert({
        "judul_berita": data.judul_berita,
        "deskripsi_berita": data.deskripsi_berita,
        "isi_berita": data.isi_berita,
        "image_berita": data.image_berita,
        "id_kategori_berita": data.kategori_berita,
        "status": "1",
        "timestamp": new Date()
      })
      .then(result => {
        resolve(result)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function UpdateBerita(data) {
  return new Promise((resolve, reject) => {
    db('berita')
      .where('id_berita', data.id_berita)
      .update({
        "judul_berita": data.judul_berita,
        "deskripsi_berita": data.deskripsi_berita,
        "isi_berita": data.isi_berita,
        "id_kategori_berita": data.kategori_berita,
        "image_berita": data.image_berita,
        "timestamp": new Date()
      })
      .then(result => {
        resolve(result)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function DeleteBerita(data) {
  return new Promise((resolve,reject) => {
    db('berita')
    .where('id_berita', data.id_berita)
    .update({
      "status" : "0",
      "timestamp" : new Date()
    })
    .then(result => {
      resolve(result)
    })
    .catch(error => {
      reject(error)
    })
  })
}

function BeritaAgama(callback, limit, offset) {
  return new Promise((resolve, reject) => {
    db
      .select(
      'a.id_berita', 'a.judul_berita', 'a.deskripsi_berita', 'a.isi_berita',
      'a.image_berita', 'b.nama_kategori_berita', 'a.timestamp')
      .from('berita AS a')
      .leftJoin('kategori_berita AS b', 'a.id_kategori_berita', 'b.id_kategori_berita')
      .where({
        'a.id_kategori_berita': 1,
        'a.status': 1
      })
      .then(res => {
        db
          .select('a.id_berita', 'a.judul_berita', 'a.deskripsi_berita', 'a.isi_berita',
          'a.image_berita', 'b.nama_kategori_berita', 'a.timestamp')
          .from('berita AS a')
          .leftJoin('kategori_berita AS b', 'a.id_kategori_berita', 'b.id_kategori_berita')
          .where({
            'a.id_kategori_berita': 1,
            'a.status': 1
          })
          .limit(limit)
          .offset(offset)
          .then(result => {
            callback(null, result, res.length);
          })
          .catch(error => {
            callback(null, error);
          })
        //callback(null, result)
      })
      .catch(function (error) {
        callback(null, error)
      })
  })
}

function BeritaHumaniora(callback, limit, offset) {
  return new Promise((resolve, reject) => {
    db
      .select('a.id_berita', 'a.judul_berita', 'a.deskripsi_berita', 'a.isi_berita',
      'a.image_berita', 'b.nama_kategori_berita', 'a.timestamp')
      .from('berita AS a')
      .leftJoin('kategori_berita AS b', 'a.id_kategori_berita', 'b.id_kategori_berita')
      .where({
        'a.id_kategori_berita': 2,
        'a.status': 1
      })
      .then(res => {
        db
          .select('a.id_berita', 'a.judul_berita', 'a.deskripsi_berita', 'a.isi_berita',
          'a.image_berita', 'b.nama_kategori_berita', 'a.timestamp')
          .from('berita AS a')
          .leftJoin('kategori_berita AS b', 'a.id_kategori_berita', 'b.id_kategori_berita')
          .where({
            'a.id_kategori_berita': 2,
            'a.status': 1
          })
          .limit(limit)
          .offset(offset)
          .then(result => {
            callback(null, result, res.length);
          })
          .catch(error => {
            callback(null, error);
          })
      })
      .catch(error => {
        callback(null, error)
      })
  })
}

function BeritaPendidikan(callback, limit, offset) {
  return new Promise((resolve, reject) => {
    db
      .select('a.id_berita', 'a.judul_berita', 'a.deskripsi_berita', 'a.isi_berita',
      'a.image_berita', 'b.nama_kategori_berita', 'a.timestamp')
      .from('berita AS a')
      .leftJoin('kategori_berita AS b', 'a.id_kategori_berita', 'b.id_kategori_berita')
      .where({
        'a.id_kategori_berita': 3,
        'a.status': 1
      })
      .then(res => {
        db
          .select('a.id_berita', 'a.judul_berita', 'a.deskripsi_berita', 'a.isi_berita',
          'a.image_berita', 'b.nama_kategori_berita', 'a.timestamp')
          .from('berita AS a')
          .leftJoin('kategori_berita AS b', 'a.id_kategori_berita', 'b.id_kategori_berita')
          .where({
            'a.id_kategori_berita': 3,
            'a.status': 1
          })
          .limit(limit)
          .offset(offset)
          .then(result => {
            callback(null, result, res.length);
          })
          .catch(error => {
            callback(null, error);
          })

      })
      .catch(error => {
        callback(null, error)
      })
  })
}

function BeritaSosisal(callback, limit, offset) {
  return new Promise((resolve, reject) => {
    db
      .select('a.id_berita', 'a.judul_berita', 'a.deskripsi_berita', 'a.isi_berita',
      'a.image_berita', 'b.nama_kategori_berita', 'a.timestamp')
      .from('berita AS a')
      .leftJoin('kategori_berita AS b', 'a.id_kategori_berita', 'b.id_kategori_berita')
      .where({
        'a.id_kategori_berita': 4,
        'a.status': 1
      })
      .then(res => {
        db
          .select('a.id_berita', 'a.judul_berita', 'a.deskripsi_berita', 'a.isi_berita',
          'a.image_berita', 'b.nama_kategori_berita', 'a.timestamp')
          .from('berita AS a')
          .leftJoin('kategori_berita AS b', 'a.id_kategori_berita', 'b.id_kategori_berita')
          .where({
            'a.id_kategori_berita': 4,
            'a.status': 1
          })
          .limit(limit)
          .offset(offset)
          .then(result => {
            callback(null, result, res.length);
          })
          .catch(error => {
            callback(null, error);
          })
      })
      .catch(error => {
        callback(null, error)
      })
  })
}

function AllBerita(callback, limit, offset) {
  db
    .select('id_berita', 'judul_berita', 'deskripsi_berita', 'isi_berita', 'image_berita', 'id_kategori_berita', 'timestamp')
    .from('berita')
    .where('status', 1)
    .then(function (res) {
      db
        .select('id_berita', 'judul_berita', 'deskripsi_berita', 'isi_berita', 'id_kategori_berita', 'image_berita', 'timestamp')
        .from('berita')
        .where('status', 1)
        .limit(limit)
        .offset(offset)
        .then(function (result) {
          callback(null, result, res.length);
        })
        .catch(function (error) {
          callback(null, error);
        })
    })
    .catch(function (error) {
      callback(null, error);
    })
}

function FindBerita(id, callback) {
  return new Promise((resolve, reject) => {
    db
      .select(
      'id_berita',
      'judul_berita',
      'deskripsi_berita',
      'isi_berita',
      'image_berita',
      'timestamp')
      .from('berita')
      .where('id_berita', id)
      .then(function (result) {
        callback(null, result)
      })
      .catch(function (error) {
        callback(null, error)
      })
  })
}

function BeritaHome(callback) {
  return new Promise((resolve, reject) => {
    db
      .select('id_berita','judul_berita', 'deskripsi_berita','isi_berita','image_berita')
      .from('berita')
      .limit(8)
      .where('status', 1)
      .orderBy('id_berita', 'desc')
      .then(result => {
        callback(null, result)
      })
      .catch(error => {
        callback(null, error)
      })

  })
}

module.exports = {
  AddBerita: AddBerita,
  BeritaAgama: BeritaAgama,
  BeritaHumaniora: BeritaHumaniora,
  BeritaPendidikan: BeritaPendidikan,
  BeritaSosisal: BeritaSosisal,
  AllBerita: AllBerita,
  FindBerita: FindBerita,
  UpdateBerita: UpdateBerita,
  DeleteBerita: DeleteBerita,
  BeritaHome: BeritaHome
}