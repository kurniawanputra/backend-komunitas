var express = require('express');
var app = express();
var knex = require('knex');
var bosyparser = require('body-parser');
var db = require('../koneksi/connection');


function AddSaran(data) {
  return new Promise((resolve, reject) => {
    db('saran')
      .insert({
        "nama" : data.nama,
        "email" : data.email,
        "subyek" : data.subyek,
        "pesan" : data.pesan,
        "timestamp" : new Date()
      })
      .then(result => {
        resolve(result)
      })
      .catch(error => {
        reject(error)
      })
  })
}

module.exports = {
  AddSaran    : AddSaran
}