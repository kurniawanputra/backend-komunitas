var express = require('express');
var app = express();
var knex = require('knex');
var bosyparser = require('body-parser');
var db = require('../koneksi/connection');

function addGallery(data) {
  return new Promise((resolve, reject) => {
    data.forEach((d) => {
      d.timestamp = new Date();
      d.status = "1";
    });

    db('gallery')
      .insert(data)
      .then(result => {
        resolve(result)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function GalleryHome(callback) {
  return new Promise((resolve,reject) => {
    db
      .select('id_gallery','url_gallery','judul_gallery')
      .from('gallery')
      .limit(14)
      .where('status', 1)
      .orderBy('id_gallery', 'desc')
      .then(function (result) {
        callback(null, result)
      })
      .catch(function (error) {
        callback(null, error)
      })
  })
}

function listGallery(callback, limit, offset) {
  db
    .select('id_gallery', 'judul_gallery', 'url_gallery', 'timestamp')
    .from('gallery')
    .where('status', 1)
    .then(function (res) {
      db
        .select('id_gallery', 'judul_gallery', 'url_gallery', 'timestamp')
        .from('gallery')
        .where('status', 1)
        .limit(limit)
        .offset(offset)
        .then(function (result) {
          callback(null, result, res.length);
        })
        .catch(function (error) {
          callback(null, error);
        })

    })
    .catch(function (error) {
      callback(null, error);
    })
}



module.exports = {
  addGallery: addGallery,
  listGallery: listGallery,
  GalleryHome: GalleryHome
}