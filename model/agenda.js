var express = require('express');
var app = express();
var knex = require('knex');
var bosyparser = require('body-parser');
var db = require('../koneksi/connection');
var moment = require('moment');

function viewAgenda(callback, limit, offset) {
  db
    .select('id_agenda', 'title', 'start_date', 'end_date', 'desc')
    .from('agenda')
    .where('status', 1)
    .then(function (res) {
      db
        .select('id_agenda', 'title', 'start_date', 'end_date', 'desc')
        .from('agenda')
        .where('status', 1)
        .limit(limit)
        .offset(offset)
        .then(function (result) {
          callback(null, result, res.length);
        })
        .catch(function (error) {
          callback(null, error);
        })

    })
    .catch(function (error) {
      callback(null, error);
    })
}

function viewAllAgenda(callback) {
  return new Promise((resolve, reject) => {
    db
      .select(
      'id_agenda',
      'title',
      'start_date',
      'end_date',
      'desc'
      )
      .from('agenda')
      .then(function (result) {
        callback(null, result)
      })
      .catch(function (error) {
        callback(null, error)
      })
  })
}

function addAgenda(data) {
  return new Promise((resolve, reject) => {
    db('agenda')
      .insert({
        "title": data.title,
        "start_date": data.start_date,
        "end_date": data.end_date,
        "desc": data.desc,
        "status": "1",
        "timestamp": new Date()
      })
      .then(result => {
        resolve(result)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function updateAgenda(data) {
  return new Promise((resolve, reject) => {
    db('agenda')
      .where('id_agenda', data.id_agenda)
      .update({
        "title" : data.title,
        "start_date" : data.start_date,
        "end_date" : data.end_date,
        "desc" : data.desc,
        "timestamp" : new Date()
      })
      .then(result => {
        resolve(result)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function findAgenda(id, callback) {
    db
      .select(
      'id_agenda',
      'title',
      'start_date',
      'end_date',
      'desc'
      )
      .from('agenda')
      .where('id_agenda', id)
      .then(function (result) {
        callback(null, result)
      })
      .catch(function (error) {
        callback(null, error)
      })
}

function deleteAgenda(data) {
  return new Promise((resolve, reject) => {
    db('agenda')
      .where('id_agenda', data.id_agenda)
      .update({ 
        "status" : 0,
        "timestamp" : new Date()
      })
      .then(result => {
        resolve(result)
      })
      .catch(error => {
        reject(error)
      })
  })
}

module.exports = {
  viewAgenda    : viewAgenda,
  addAgenda     : addAgenda,
  viewAllAgenda : viewAllAgenda,
  findAgenda    : findAgenda,
  updateAgenda  : updateAgenda,
  deleteAgenda  : deleteAgenda
}



