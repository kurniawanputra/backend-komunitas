var express = require('express');
var app = express();
var knex = require('knex');
var bosyparser = require('body-parser');
var db = require('../koneksi/connection');
var bcrypt = require('bcrypt-nodejs');

const saltRounds = 10;
const myPlaintextPassword = 's0/\/\P4$$w0rD';

function checkLogin(data) {
  return new Promise((resolve, reject) => {
    bcrypt.genSalt(saltRounds, function (err, salt) {
      bcrypt.hash(data.password, salt, null, function (err, password) {
        console.log(password);
        if (err) {
          reject(err)
        } else {
          db('user')
            .select('username', 'password')
            .from('user')
            .where('username', data.username)
            .then(result => {
              bcrypt.compare(data.password, result[0].password, function (err, res) {
                //bcrypt.compare(password, result[0].password, function (err, res) {
                if (res === true) {
                  resolve(result)
                } else {
                  reject('gagal login')
                }
              })
            })
            .catch(error => {
              reject(error)
            })
        }
      })
    })
  })
}

function InsertUser(data) {
  console.log(data);
  return new Promise((resolve, reject) => {
    bcrypt.genSalt(saltRounds, function (err, salt) {
      bcrypt.hash(data.password, salt, null, function (err, hash) {
        if (err) {
          reject(err)
        } else {
          db('user')
            .insert({
              "username": data.username,
              "password": hash,
              "status": "1",
              "timestamp": new Date()
            })
            .then(result => {
              resolve(result)
            })
            .catch(error => {
              reject(error)
            })
        }
      });
    });
  })
}

function updateUser(data) {
  return new Promise((resolve, reject) => {
    bcrypt.genSalt(saltRounds, function (err, salt) {
      bcrypt.hash(data.baru, salt, null, function (err, baru) {
        bcrypt.hash(data.password, salt, null, function (err, hash) {
          console.log(hash);
          db('user')
            .select('username', 'password')
            .from('user')
            .where('username', data.username)
            .then(result => {
              console.log(result);
              bcrypt.compare(data.password, result[0].password, function (err, res) {
                console.log(res);
                if (res === true) {
                  db('user')
                    .where('id_user', data.id_user)
                    .update({
                      "password": baru,
                      "username": data.username,
                      "timestamp": new Date()
                    })
                    .then(result => {
                      resolve(result)
                    })
                    .catch(error => {
                      reject(error)
                    })
                } else {
                  reject("password tidak sama")
                }
              })
            })
            .catch(error => {
              reject(error)
            })
        })
      })
    })
  })
}

function listUser(callback, limit, offset) {
  db
    .select('id_user', 'username')
    .from('user')
    .where('status', 1)
    .then(function (res) {
      db
        .select('id_user', 'username')
        .from('user')
        .where('status', 1)
        .limit(limit)
        .offset(offset)
        .then(function (result) {
          callback(null, result, res.length);
        })
        .catch(function (error) {
          callback(null, error);
        })

    })
    .catch(function (error) {
      callback(null, error);
    })
}

function findUser(id, callback) {
  return new Promise((resolve, reject) => {
    db
      .select(
        'id_user',
        'username'
      )
      .from('user')
      .where('id_user', id)
      .then((result) => {
        callback(null, result)
      })
      .catch((error) => {
        callback(null, error)
      })
  })
}

function deleteUser(data) {
  return new Promise((resolve, rejcet) => {
    db('user')
      .where('id_user', data.id_user)
      .update({
        "status": "0",
        "timestamp": new Date()
      })
      .then(result => {
        resolve(result)
      })
      .catch(error => {
        reject(error)
      })
  })
}


module.exports = {
  InsertUser: InsertUser,
  listUser: listUser,
  findUser: findUser,
  updateUser: updateUser,
  checkLogin: checkLogin,
  deleteUser: deleteUser
}
