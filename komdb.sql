/*
SQLyog Ultimate v8.55 
MySQL - 5.5.5-10.1.28-MariaDB : Database - komunitas_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`komunitas_db` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `komunitas_db`;

/*Table structure for table `agenda` */

DROP TABLE IF EXISTS `agenda`;

CREATE TABLE `agenda` (
  `id_agenda` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id_agenda`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `agenda` */

insert  into `agenda`(`id_agenda`,`title`,`start_date`,`end_date`,`desc`,`status`,`timestamp`) values (1,'update satu','2017-12-11 03:00:00','2017-12-13 04:00:00','update agenda satu',1,'2018-02-14 17:34:59'),(2,'testing dua','2017-11-08 21:00:00','2017-11-08 23:00:00','deskripsi dua',1,'2018-02-14 17:35:03'),(3,'testing tiga','2017-11-08 21:00:00','2017-11-12 09:00:00','testing tiga',1,NULL),(4,'testing empat','2017-11-08 21:00:00','2017-11-12 23:00:00','testing empat',1,NULL),(5,'testing satu','2017-11-08 21:00:00','2017-11-12 07:00:00','agenda satu',1,NULL),(6,'testing enam','2017-12-09 03:00:00','2017-12-09 03:00:00','miting ke enam',1,NULL),(7,'testing tujuh','2017-12-09 03:00:00','2017-12-10 15:00:00','meting ke tujuh',1,NULL),(8,'testing dellapan','2017-12-10 10:00:00','2017-12-10 23:00:00','meting kedelapan',1,NULL),(9,'testing sembilan','2017-12-10 10:20:00','2017-12-10 22:35:00','miting ke sembilan',1,NULL),(10,'testing update sepuluh','2017-12-09 22:25:00','2017-12-11 10:47:00','miting update sepuluh',0,'2018-03-14 13:35:40'),(13,'update coba','2017-12-18 03:00:00','2017-12-19 11:00:00','update desc',1,NULL);

/*Table structure for table `berita` */

DROP TABLE IF EXISTS `berita`;

CREATE TABLE `berita` (
  `id_berita` int(11) NOT NULL AUTO_INCREMENT,
  `judul_berita` varchar(200) DEFAULT NULL,
  `deskripsi_berita` varchar(200) DEFAULT NULL,
  `isi_berita` varchar(500) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `image_berita` varchar(200) DEFAULT NULL,
  `id_kategori_berita` int(11) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id_berita`),
  KEY `FK_berita` (`id_kategori_berita`),
  CONSTRAINT `FK_berita` FOREIGN KEY (`id_kategori_berita`) REFERENCES `kategori_berita` (`id_kategori_berita`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;

/*Data for the table `berita` */

insert  into `berita`(`id_berita`,`judul_berita`,`deskripsi_berita`,`isi_berita`,`status`,`deleted_at`,`image_berita`,`id_kategori_berita`,`timestamp`) values (28,'berita agama satu','deskripsi tiga','lorem ispum lorem ispum lorem ispum lorem ispum lorem ispum lorem ispum',1,NULL,'c.jpg',4,'2018-02-14 15:55:50'),(29,'berita humaniora 1','deskripsi tiga','lorem ispum lorem ispum lorem ispum lorem ispum lorem ispum lorem ispum',1,NULL,'a.jpg',2,'2018-02-14 15:55:59'),(30,'berita agama dua','berita agama dua','lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsumlorem ipsum lorem ipsumlorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsumlorem ipsum lorem ipsumlorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsumlorem ipsum lorem ipsumlorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lo',1,NULL,'b.jpg',1,'2018-02-14 15:56:05'),(31,'berita agama tiga','berita agama tiga','lorem ispum lorem ispum lorem ispum lorem ispum lorem ispum lorem ispum lorem ispum lorem ispum lorem ispum lorem ispum lorem ispum lorem ispumlorem ispum lorem ispum lorem ispum lorem ispumlorem ispum lorem ispum lorem ispum lorem ispum',1,NULL,'c.jpg',1,'2018-02-14 16:04:30'),(32,'berita humaniora 2','berita humaniora 2','lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsumlorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsumlorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsumlorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum',1,NULL,'b.jpg',2,'2018-02-14 16:04:34'),(33,'berita humaniora 3','berita humaniora 3','lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum',1,NULL,'c.jpg',2,'2018-02-14 16:04:52'),(34,'berita pendidikan 1','berita pendidikan 1','lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsumlorem ipsum lorem ipsumlorem ipsum lorem ipsumlorem ipsum lorem ipsumlorem ipsum lorem ipsumlorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsumlorem ipsum lorem ipsumlorem ipsum lorem ipsumlorem ipsum lorem ipsumlorem ipsum lorem ipsumlorem ipsum lorem ipsumlorem ipsum lorem ipsum',1,NULL,'a.jpg',3,'2017-12-25 19:09:29'),(35,'berita pendidikan 2','berita pendidikan 2','lorem ipsum lorem ipsum lorem ipsum lorem ipsumlorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsumlorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsumlorem ipsumlorem ipsum lorem ipsum lorem ipsum lorem ipsumlorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsumlorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsumlorem ipsum',1,NULL,'b.jpg',3,'2017-12-25 19:10:09'),(36,'berita pendidikan 3','berita pendidikan 3','lorem ipsum lorem ipsum lorem ipsum lorem ipsumlorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsumlorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsumlorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsumlorem ipsumlorem ipsum lorem ipsum lorem ipsum lorem ipsumlorem ipsumlorem ipsum lorem ipsum lorem ipsum lorem ipsumlorem ipsum',1,NULL,'c.jpg',3,'2017-12-25 19:10:50'),(37,'berita sosial 1','berita sosial 1','lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsumlorem ipsum lorem ipsumlorem ipsum lorem ipsumlorem ipsum lorem ipsumlorem ipsum lorem ipsum',0,NULL,'a.jpg',4,'2018-03-14 13:35:24'),(38,'berita sosial 2','berita sosial 2','lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum',1,NULL,'b.jpg',4,'2017-12-25 19:12:46'),(39,'berita sosial 3','berita sosial 3','lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsumlorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsumlorem ipsum lorem ipsum',1,NULL,'c.jpg',4,'2017-12-25 19:13:24'),(40,'judul empat puluh akh','deskripsi empat puluh akh','isi empat puluh akh',1,NULL,'coba.jpg',3,'2018-02-06 17:59:35'),(41,'judul empat puluh','deskripsi empat puluh','isi empat puluh',1,NULL,'b.jpg',3,'2018-02-05 19:28:24'),(42,'berita 5','deskripsi 5','isi berita 5',1,NULL,'c.jpg',1,'2018-02-05 14:57:59'),(43,'berita 6','deskripsi 6','isi berita 6',1,NULL,'a.jpg',1,'2018-02-05 14:58:38'),(44,'berita 7','deskripsi berita 7','isi berita 7',1,NULL,'b.jpg',1,'2018-02-05 14:59:15'),(45,'berita 8','deskripsi 8','isi berita 8',1,NULL,'c.jpg',1,'2018-02-05 14:59:44'),(46,'berita 9','deskripsi 9','isi berita 9',1,NULL,'c.jpg',1,'2018-02-05 15:00:11'),(47,'berita 10','deskripsi 10','isi berita 10',1,NULL,'coba.jpg',1,'2018-02-05 15:01:28'),(48,'berita 11','deskripsi 11','isi berita 11',1,NULL,'c.jpg',1,'2018-02-05 15:01:56'),(49,'berita 5','deskripsi 5','isi berita 5',1,NULL,'a.jpg',2,'2018-02-05 16:24:59'),(50,'berita 6','deskripsi 6','isi berita 6',1,NULL,'b.jpg',2,'2018-02-05 16:25:27'),(51,'berita 7','deskripsi 7','isi berita 7',1,NULL,'c.jpg',2,'2018-02-05 16:26:10'),(52,'berita 8','deskripsi 8','isi berita 8',1,NULL,'a.jpg',2,'2018-02-05 16:26:44'),(53,'berita 9','deskripsi 9','isi berita 9',1,NULL,'coba.jpg',2,'2018-02-05 16:27:18'),(54,'berita 11','deskripsi 11','isi berita 11',1,NULL,'c.jpg',2,'2018-02-05 16:27:53'),(55,'berita sebelas','deskripsi 11','isi berita 11',1,NULL,'c.jpg',2,'2018-02-05 16:28:39'),(56,'berita 12','deskripsi 12','isi berita 12',1,NULL,'c.jpg',2,'2018-02-05 16:31:05'),(57,'berita dua belas','deskripsi 12','isi berita dua belas',1,NULL,'c.jpg',2,'2018-02-05 16:59:50'),(58,'berita 4','deskripsi 4','isi berita 4',1,NULL,'a.jpg',3,'2018-02-05 17:35:16'),(59,'berita 5','deskripsi 5','isi berita 5',1,NULL,'b.jpg',3,'2018-02-05 17:36:11'),(60,'berita 5','deskripsi 5','isi berita 5',1,NULL,'b.jpg',3,'2018-02-05 17:36:45'),(61,'berita 7','deskripsi 7','isi berita 7',1,NULL,'c.jpg',3,'2018-02-05 17:37:12'),(62,'berita 8','deskripsi 8','isi berita 8',1,NULL,'coba.jpg',3,'2018-02-05 17:37:53'),(63,'berita 9','deskripsi 9','isi berita 9',1,NULL,'b.jpg',3,'2018-02-05 17:38:24'),(64,'berita 10','deskripsi 10','isi berita 10',1,NULL,'c.jpg',3,'2018-02-05 17:38:55'),(65,'berita 11','deskripsi 11','isi berita 11',1,NULL,'a.jpg',3,'2018-02-05 17:39:21'),(66,'berita 12','deskripsi 12','isi berita 12',1,NULL,'c.jpg',3,'2018-02-05 17:39:44');

/*Table structure for table `gallery` */

DROP TABLE IF EXISTS `gallery`;

CREATE TABLE `gallery` (
  `id_gallery` int(11) NOT NULL AUTO_INCREMENT,
  `judul_gallery` varchar(200) DEFAULT NULL,
  `url_gallery` varchar(200) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_gallery`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=latin1;

/*Data for the table `gallery` */

insert  into `gallery`(`id_gallery`,`judul_gallery`,`url_gallery`,`timestamp`,`status`) values (51,'a.jpg','511f5678d715a0f8146b5fce106c7f5d.jpg','0000-00-00 00:00:00',1),(52,'b.jpg','3ab1c3564eba92a78e52bb15ebded906.jpg','0000-00-00 00:00:00',1),(53,'c.jpg','713e6b3b49a2bcbb1a8edfd932c3d46e.jpg','0000-00-00 00:00:00',1),(54,'a.jpg','8aac612cb6bd81ca804d1c8555a34820.jpg','0000-00-00 00:00:00',1),(55,'b.jpg','e00fc54ba55e42bf597826cc00b1272e.jpg','0000-00-00 00:00:00',1),(57,'a.jpg','5fee0abf2aa83046cef44943d9c137ac.jpg','0000-00-00 00:00:00',1),(58,'a.jpg','32c2595184d893d5d7668923b66b144c.jpg','0000-00-00 00:00:00',1),(59,'b.jpg','8a6f30d848f267d5682c1fc5e28cfa0d.jpg','0000-00-00 00:00:00',1),(60,'a.jpg','35ad940a44282bfa385d7d6d21375b62.jpg','0000-00-00 00:00:00',1),(61,'a.jpg','c29be9adefa44b4d5f74f99c1d253597.jpg','0000-00-00 00:00:00',1),(62,'b.jpg','7818fb4ad4a3ecd53ba6b1f5bb4863f1.jpg','0000-00-00 00:00:00',1),(63,'a.jpg','70d6a7b80d12a3d6b1e5d2c2f1138f20.jpg','0000-00-00 00:00:00',1),(64,'b.jpg','a054f308d0e8da9ebb4fbe0d5241b142.jpg','0000-00-00 00:00:00',1),(65,'a.jpg','f050244799a0f9e6b53215980ac49909.jpg','0000-00-00 00:00:00',1),(66,'a.jpg','2261f7eac384c874a676f7ecd6fab6dc.jpg','0000-00-00 00:00:00',1),(67,'b.jpg','2a756c86ddafa6d378042d416265e461.jpg','0000-00-00 00:00:00',1),(68,'b.jpg','09ea28a0b60d3717f678b5ed7a9ae8f4.jpg','0000-00-00 00:00:00',1),(69,'c.jpg','2e1979363bf445b85d5a28ed02e80472.jpg','0000-00-00 00:00:00',1);

/*Table structure for table `kategori_berita` */

DROP TABLE IF EXISTS `kategori_berita`;

CREATE TABLE `kategori_berita` (
  `id_kategori_berita` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori_berita` varchar(200) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_kategori_berita`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `kategori_berita` */

insert  into `kategori_berita`(`id_kategori_berita`,`nama_kategori_berita`,`status`) values (1,'Agama',1),(2,'Humaniora',1),(3,'Pendidikan',1),(4,'Sosial',1);

/*Table structure for table `saran` */

DROP TABLE IF EXISTS `saran`;

CREATE TABLE `saran` (
  `id_saran` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `subyek` varchar(100) DEFAULT NULL,
  `pesan` varchar(155) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id_saran`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `saran` */

insert  into `saran`(`id_saran`,`nama`,`email`,`subyek`,`pesan`,`timestamp`) values (1,'wawan','wawan@gmail.com','testing','ini hanya testing','2018-02-17 13:02:37');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`id_user`,`username`,`password`,`status`,`timestamp`) values (1,'wawan','$2a$10$EatPGAaYTWTE12lQZXizB.KZ5xE1znA8rKRIYDEGGw.OJjY7.akoG',0,'2018-03-13 16:43:02'),(2,'testing','$2a$10$Hy1Ys4BW06iLINkT6XZ5MObq2Olya1DTsAUIHv9aVtzQWetWX2FOC',1,'2018-01-05 15:04:28'),(3,'testingsaja','$2a$10$6omhG5YEsLBxx89P4VBFbePi3MFd75.24NqX.k3O6UWqoiJioY.ry',1,'2018-01-05 19:05:53'),(4,'123','$2a$10$95ldTM08jAug5sIKZ.NN7uzRSleIspMibGviuoSR1.K//1IovNDsO',1,'2018-01-05 19:31:12'),(5,'baskoro','$2a$10$0UTZyL0d8H0WgiZHXrvqWeR0JI9.wY/hXfSgxzzxZmZj0PWSeCs6C',1,'2018-01-05 19:31:58'),(8,'dimas','$2a$10$p7W2Yn0l9gvXz9lmd0rOc.Ai5wx3gkhsRFK8qVwlHYOvPvCOFYJXa',1,'2018-02-11 09:52:50'),(9,'raedi','$2a$10$1BpQNZKiq0BGrgzd6XQrqe5TkqOf.hH9TtcNSB0cd0lr4mmX50IcC',1,'2018-01-19 21:21:22'),(10,'adi','$2a$10$n1jl8DVPNzZ8.ascRbaPCO5U4GkiGRZb25WInseSPGx4G0B3qwn86',1,'2018-02-11 10:05:51'),(11,'derina','$2a$10$9dTZPxL3QUqzkG7/HENMrOTWEvXLsOacpvGclIcLqWMvK8qu0x5Ee',0,'2018-02-14 15:53:16'),(12,'vina','$2a$10$BtSUOr//qGjKGx3GKbZ40eznhi6XG8Rh4kyaoyAAZLhSCPzC63bWC',0,'2018-02-14 15:11:58'),(14,'aang','$2a$10$LDGM6/q6etiHOiWKogj56u1uJ9fbIaFvAzfoab7jkGEq8K6P1/kKO',1,'2018-02-21 19:29:11'),(15,'naufal','$2a$10$65hUeQ8PpV/uXhZkw6ISCO5dh12MqZfj0CsBK5kyxuO/qFFq.OX0K',1,'2018-02-22 13:56:56'),(16,'testing','$2a$10$xFujcTJmaX/F4dfHjOAL/.K8HzH7mEZOWEVXQIIpog5AHyV6ed7j2',0,'2018-03-14 13:35:04');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
